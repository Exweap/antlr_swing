grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    | PRINT expr NL -> ^(PRINT expr)
    | if_expr NL -> if_expr
    | while_loop NL -> while_loop
    | do_while_loop NL -> do_while_loop
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | LB
    | RB
    | NL ->
    ;

if_expr
    : IF^ condition THEN! expr (ELSE! expr)? END!;

while_loop
    : WHILE^ condition THEN! expr
    ;

do_while_loop
    : DO^ expr WHILE! condition
    ;

condition
    : expr (IS_EQUAL^ expr | NOT_EQUAL^ expr)*;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | SR^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

PRINT
  : 'print'
  ; 

VAR 
  : 'var'
  ;

IF
  : 'if'
  ;

THEN
  : 'then'
  ;

ELSE
  : 'else'
  ; 

END
  : 'end'
  ;

WHILE
  : 'while'
  ;

DO
  : 'do'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;
	
LB
  : '{'
  ;

RB
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

SR
  : '>>'
  ;

IS_EQUAL
  : '=='
  ;

NOT_EQUAL
  : '!='
  ;
  