package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private LocalSymbols symbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    public Integer add(Integer e1, Integer e2) {
    	return e1 + e2;
    }
    
    public Integer sub(Integer e1, Integer e2) {
    	return e1 - e2;
    }
    
    public Integer mul(Integer e1, Integer e2) {
    	return e1 * e2;
    }
    
    public Integer div(Integer e1, Integer e2) throws Exception {
    	if(e2 == 0)
    		throw new Exception("Could not divide by 0.");
    	return e1 / e2;
    }
    
    public Integer sr(Integer e1, Integer e2) {
    	return e1 >> e2;
    }
    
    public void newVar(String s) {
        symbols.newSymbol(s);
    }
    
    public Integer setVar(String s, Integer e) {
        symbols.setSymbol(s, e);
        return e;
    }
    
    public Integer getVar(String s) {
        return symbols.getSymbol(s);
    }
    
    public Integer startBlock() {
        return symbols.enterScope();
    }
    
    public Integer endBlock() {
        return symbols.leaveScope();
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
