tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr | vars | output)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(SR    e1=expr e2=expr) {$out = sr($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVar($ID.text);}
        | LB                       {startBlock();}
        | RB                       {endBlock();}
        ;
catch [Exception excpt] {drukuj("Exception ocurred: " + excpt.getMessage()); $out=Integer.MAX_VALUE;}
        
vars
        : ^(VAR i1=ID)             {newVar($i1.text);}
          ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out);}
        ;

output
        : ^(PRINT e=expr)          {drukuj($e.text + " = " + $e.out.toString());}
        ;