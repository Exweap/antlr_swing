tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer ifCounter = 0;
  Integer whileCounter = 0;
  Integer doWhileCounter = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},declarations={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> declVar(var={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {varExists($i1.text);} -> setVar(var={$i1.text}, value={$e2.st})
        | ID {varExists($ID.text);} -> getVar(var={$ID.text})
        | INT  {numer++;} -> int(i={$INT.text},j={numer.toString()})
        | ^(IF e1=expr e2=expr e3=expr?) {ifCounter++;}  -> if(cond={$e1.st}, p1={$e2.st}, p2={$e3.st}, counter={ifCounter.toString()})
        | ^(IS_EQUAL e1=expr e2=expr) -> is_equals(p1={$e1.st},p2={$e2.st})
        | ^(NOT_EQUAL e1=expr e2=expr) -> not_equal(p1={$e1.st},p2={$e2.st})
        | ^(WHILE e1=expr e2=expr) {whileCounter++;} -> while_loop(p1={$e1.st}, p2={$e2.st}, counter={whileCounter.toString()})
        | ^(DO e1=expr e2=expr) {doWhileCounter++;} -> do_while_loop(p1={$e1.st}, p2={$e2.st}, counter={whileCounter.toString()})
    ;
